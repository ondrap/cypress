//
// Created on 6/7/2019.
//

#ifndef __REDIRECT_GLOBAL_INSTANCES_HPP
#define __REDIRECT_GLOBAL_INSTANCES_HPP

namespace shared {
namespace drivers{

  extern class UART uart;

}}

void InitSystem();

#endif //__REDIRECT_GLOBAL_INSTANCES_HPP
