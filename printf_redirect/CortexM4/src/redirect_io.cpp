//
// Created on 6/7/2019.
//

/***************************************************************************//**
* \file retarget.c
* \version 1.10
*
* \brief
* 'Retarget' layer for target-dependent low level function.
*
********************************************************************************
* \copyright
* Copyright 2016-2018, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include <cstdio>
#include "global_instances.hpp"
#include "shared/drivers/uart.hpp"

using namespace shared::drivers;

/* Add an explicit reference to the floating point printf library to allow
the usage of floating point conversion specifier. */
__asm (".global _printf_float");
/***************************************************************************
* Function Name: _write
***************************************************************************/
extern "C" int _write (int fd, const char *ptr, int len)
{
  uart.Write(ptr ,len);
  return len;
}

/* Add an explicit reference to the floating point scanf library to allow
the usage of floating point conversion specifier. */
__asm (".global _scanf_float");
extern "C" int _read (int fd, char *ptr, int len)
{
  uart.Read(ptr, len);
  return len;
}

/* [] END OF FILE */





