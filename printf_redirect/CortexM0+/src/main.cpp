#include "system_psoc6.h"
#include "cy_gpio.h"

int main(void)
{
    __enable_irq(); /* Enable global interrupts. */
    Cy_GPIO_Pin_FastInit(P6_3_PORT, P6_3_NUM, CY_GPIO_DM_STRONG_IN_OFF, 0, P6_3_GPIO);

    /* Enable CM4. CY_CORTEX_M4_APPL_ADDR must be updated if CM4 memory
     * layout is changed. */
    Cy_SysEnableCM4(CY_CORTEX_M4_APPL_ADDR);

    for(;;){
        Cy_GPIO_Inv(P6_3_PORT, P6_3_NUM);
        Cy_SysLib_Delay(500);
    }
}

/* [] END OF FILE */
