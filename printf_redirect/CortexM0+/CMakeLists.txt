cmake_minimum_required(VERSION 3.7.2)

######################################################################
#
#	Project
#
######################################################################

project(printf_redirect_cortex_m0plus)
enable_language(ASM)

######################################################################
#
#	CPP Definitions
#
######################################################################

set(PROJECT_FLAGS_ARCH
    "-mcpu=cortex-m0plus \
     -mthumb"
)

set(PROJECT_LINKER_FLAGS
    "-Wl,-Map=${PROJECT_NAME}.map \
    -L${PROJECT_SOURCE_DIR}/../config/ \
    -Tcy8c6xx7_cm0plus.ld"
)

set(PROJECT_C_FLAGS_WARNINGS   "${COMMON_FLAGS_WARNINGS} ${C_FLAGS_WARNINGS_EXTRA}")
set(PROJECT_CXX_FLAGS_WARNINGS "${COMMON_FLAGS_WARNINGS}")

######################################################################
#
#	Sources
#
######################################################################

set(PROJECT_INCLUDE_DIRS
    ${PROJECT_CONFIG_DIR}
)

set(PROJECT_SRCS
    ${PROJECT_SOURCE_DIR}/src/main.cpp

    ${CYPRESS_PERIPHERAL_DRIVERS_LIBRARY}/devices/templates/COMPONENT_MTB/COMPONENT_CM0P/system_psoc6_cm0plus.c
    ${CYPRESS_PERIPHERAL_DRIVERS_LIBRARY}/devices/templates/COMPONENT_MTB/COMPONENT_CM0P/TOOLCHAIN_GCC_ARM/startup_psoc6_01_cm0plus.S
)

######################################################################
#
#	TARGET
#
######################################################################

set (CMAKE_C_FLAGS   "${C_FLAGS} ${C_FLAGS_WARNINGS}  ${FLAGS_OPTIMIZATION} ${PROJECT_FLAGS_ARCH}")
set (CMAKE_ASM_FLAGS "${C_FLAGS}   ${C_FLAGS_WARNINGS}   ${FLAGS_OPTIMIZATION} ${PROJECT_FLAGS_ARCH}")
set (CMAKE_CXX_FLAGS "${CXX_FLAGS} ${CXX_FLAGS_WARNINGS} ${FLAGS_OPTIMIZATION} ${PROJECT_FLAGS_ARCH}")
set (CMAKE_EXE_LINKER_FLAGS "${LINKER_FLAGS} ${PROJECT_LINKER_FLAGS} ${FLAGS_OPTIMIZATION} ${PROJECT_FLAGS_ARCH}")

include_directories(
        ${PROJECT_INCLUDE_DIRS}
        ${CYPRESS_PERIPHERAL_DRIVERS_LIBRARY_INCLUDE_DIRS}
        ${CYPRESS_CORE_LIBRARY}/include
)

set(M0PLUS_EXECUTABLE_NAME "${PROJECT_NAME}")
#Propagate name to upper scope
set(M0PLUS_EXECUTABLE_NAME "${M0PLUS_EXECUTABLE_NAME}" PARENT_SCOPE)

add_executable(${M0PLUS_EXECUTABLE_NAME}.elf ${PROJECT_SRCS} ${CYPRESS_PERIPHERAL_DRIVERS_LIBRARY_SRCS})