//
// Created on 6/7/2019.
//

#ifndef __REDIRECT_UART_CONFIGURATION_HPP
#define __REDIRECT_UART_CONFIGURATION_HPP

#include "shared/configuration/clock_configuration.hpp"
#include "shared/configuration/pin_configuration.hpp"
#include "shared/configuration/interrupt_configuration.hpp"

namespace shared{
  namespace configuration{

    struct UART{
      using UART_ISR = void (*)();

      CySCB_Type *scb;

      Pin tx;
      Pin rx;

      Clock clock;

      Interrupt interrupt;
      UART_ISR uartIsr;
    };

  }
}

#endif //__REDIRECT_UART_CONFIGURATION_HPP
