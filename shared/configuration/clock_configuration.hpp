//
// Created on 6/7/2019.
//

#ifndef __REDIRECT_CLOCK_CONFIGURATION_HPP
#define __REDIRECT_CLOCK_CONFIGURATION_HPP

#include "cy_sysclk.h"

namespace shared{
  namespace configuration{

    struct Clock{
      en_clk_dst_t destination;

      struct{
        cy_en_divider_types_t type;
        uint32_t number;
        uint32_t division;
      } divider;
    };

  }
}

#endif //__REDIRECT_CLOCK_CONFIGURATION_HPP
