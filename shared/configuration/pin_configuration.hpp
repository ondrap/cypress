//
// Created on 6/7/2019.
//

#ifndef __REDIRECT_PIN_CONFIGURATION_HPP
#define __REDIRECT_PIN_CONFIGURATION_HPP

#include "cy_gpio.h"

namespace shared{
namespace configuration{

  struct Pin{
    GPIO_PRT_Type* port;
    uint32_t pin;
    en_hsiom_sel_t hsiom;
  };

}
}

#endif //__REDIRECT_PIN_CONFIGURATION_HPP
