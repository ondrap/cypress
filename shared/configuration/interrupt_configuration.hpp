//
// Created on 6/7/2019.
//

#ifndef __REDIRECT_INTERRUPT_CONFIGURATION_HPP
#define __REDIRECT_INTERRUPT_CONFIGURATION_HPP

namespace shared{
  namespace configuration{

    struct Interrupt{
      IRQn_Type source;
      uint32_t  priority;
    };

  }
}

#endif //__REDIRECT_INTERRUPT_CONFIGURATION_HPP
