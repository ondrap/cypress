//
// Created on 5/23/2019.
//

#include "cy_sysclk.h"
#include "cy_sysint.h"
#include "cy_scb_uart.h"
#include "cy_gpio.h"
#include "uart.hpp"

using namespace shared::drivers;

UART::UART(configuration::UART const &configuration)
  : configuration_(configuration)
{

}

bool UART::Init() {

  // Initialize UART pins
  Cy_GPIO_Pin_FastInit(
    configuration_.tx.port,
    configuration_.tx.pin,
    CY_GPIO_DM_STRONG_IN_OFF,
    0,
    configuration_.tx.hsiom
  );

  Cy_GPIO_Pin_FastInit(
    configuration_.rx.port,
    configuration_.rx.pin,
    CY_GPIO_DM_PULLDOWN,
    0,
    configuration_.rx.hsiom
  );

  // Initialize SCB in UART mode
  cy_stc_scb_uart_config_t scbConfiguration;
  scbConfiguration.uartMode                   = CY_SCB_UART_STANDARD;
  scbConfiguration.enableMutliProcessorMode   = false;
  scbConfiguration.smartCardRetryOnNack       = false;
  scbConfiguration.irdaInvertRx               = false;
  scbConfiguration.irdaEnableLowPowerReceiver = false;
  scbConfiguration.oversample                 = 12UL;
  scbConfiguration.enableMsbFirst             = false;
  scbConfiguration.dataWidth                  = 8UL;
  scbConfiguration.parity                     = CY_SCB_UART_PARITY_NONE;
  scbConfiguration.stopBits                   = CY_SCB_UART_STOP_BITS_1;
  scbConfiguration.enableInputFilter          = false;
  scbConfiguration.breakWidth                 = 11UL;
  scbConfiguration.dropOnFrameError           = false;
  scbConfiguration.dropOnParityError          = false;
  scbConfiguration.receiverAddress            = 0UL;
  scbConfiguration.receiverAddressMask        = 0UL;
  scbConfiguration.acceptAddrInFifo           = false;
  scbConfiguration.enableCts                  = false;
  scbConfiguration.ctsPolarity                = CY_SCB_UART_ACTIVE_LOW;
  scbConfiguration.rtsRxFifoLevel             = 0UL;
  scbConfiguration.rtsPolarity                = CY_SCB_UART_ACTIVE_LOW;
  scbConfiguration.rxFifoTriggerLevel  = 0UL;
  scbConfiguration.rxFifoIntEnableMask = 0UL;
  scbConfiguration.txFifoTriggerLevel  = 0UL;
  scbConfiguration.txFifoIntEnableMask = 0UL;


  auto const initialized = Cy_SCB_UART_Init(
    configuration_.scb,
    &scbConfiguration,
    &context_
  );

  if(initialized != CY_SCB_UART_SUCCESS){
    return false;
  }

  // Configure SCB clock prescaler
  /* Connect assigned divider to be a clock source for UART */
  cy_en_sysclk_status_t clockStatus = Cy_SysClk_PeriphAssignDivider(
    configuration_.clock.destination,
    configuration_.clock.divider.type,
    configuration_.clock.divider.number
  );

  if(clockStatus != CY_SYSCLK_SUCCESS){
    return false;
  }

  clockStatus = Cy_SysClk_PeriphSetDivider(
    configuration_.clock.divider.type,
    configuration_.clock.divider.number,
    configuration_.clock.divider.division - 1
  );

  if(clockStatus != CY_SYSCLK_SUCCESS){
    return false;
  }

  clockStatus = Cy_SysClk_PeriphEnableDivider(
    configuration_.clock.divider.type,
    configuration_.clock.divider.number
  );

  if(clockStatus != CY_SYSCLK_SUCCESS){
    return false;
  }

  // Configure UART interrupt
  cy_stc_sysint_t const configurationInterrupt = {
    .intrSrc = configuration_.interrupt.source,
    .intrPriority = configuration_.interrupt.priority
  };

  auto const interruptStatus = Cy_SysInt_Init(
    &configurationInterrupt,
    configuration_.uartIsr
  );

  if(interruptStatus != CY_SYSINT_SUCCESS){
    return false;
  }

  NVIC_EnableIRQ(configuration_.interrupt.source);

  Cy_SCB_UART_Enable(configuration_.scb);

  return true;
}

uint32_t UART::Write(char const * data, uint32_t length){
  char * ptrOnConstData =
    const_cast<char *>(reinterpret_cast<char const *>(data));

  return Cy_SCB_WriteArray(
   configuration_.scb,
   ptrOnConstData,
   length
  );
}

uint32_t UART::Read(char * data, uint32_t length){
  return Cy_SCB_ReadArray(
    configuration_.scb,
    data,
    length
  );
}

void UART::UARTIsr(){
  Cy_SCB_UART_Interrupt(configuration_.scb, &context_);
}