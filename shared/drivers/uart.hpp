//
// Created on 5/23/2019.
//

#ifndef __UART_HPP
#define __UART_HPP

#include "cy_scb_common.h"
#include "cy_scb_uart.h"
#include "shared/configuration/uart_configuration.hpp"

namespace shared{
namespace drivers{

class UART{
public:
  UART(configuration::UART const &configuration);
  bool Init();
  uint32_t Write(char const * data, uint32_t length);
  uint32_t Read(char * data, uint32_t length);
private:
  void UARTIsr();

  configuration::UART const & configuration_;
  cy_stc_scb_uart_context_t context_;
public:
  template <UART &instance>
  class C_CALLBACKS{
  public:
    static void UARTIsr(){ instance.UARTIsr(); };
  };
};

}}


#endif //__UART_HPP
