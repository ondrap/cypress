if(NOT DEFINED FOREIGN_DIR)
    message(SEND_ERROR "FOREIGN_DIR isn't set.")
endif()

if(${CMAKE_HOST_SYSTEM_NAME} STREQUAL "Windows")
    set(OPENOCD_DIR ${FOREIGN_DIR}/cypress_tools_2.0/win/openocd)
elseif(${CMAKE_HOST_SYSTEM_NAME} STREQUAL "Linux")
    set(OPENOCD_DIR ${FOREIGN_DIR}/cypress_tools_2.0/linux/openocd)
elseif(${CMAKE_HOST_SYSTEM_NAME} STREQUAL "Darwin")
    set(OPENOCD_DIR ${FOREIGN_DIR}/cypress_tools_2.0/osx/openocd)
else()
    message(FATAL_ERROR "Not supported host system.")
endif()

find_program(CYPRESS_OPENOCD_EXECUTABLE openocd HINTS "${OPENOCD_DIR}/bin" NO_DEFAULT_PATH DOC "OpenOCD executable")

if(NOT EXISTS ${CYPRESS_OPENOCD_EXECUTABLE})
    message(WARNING "Missing OpenOCD executable.")
else()
    set(OPENOCD_PSOC6_COMMON_OPTIONS
            -s "${OPENOCD_DIR}/scripts"
            -c "gdb_port 3333"
            -c "tcl_port 6666"
            -c "telnet_port 4444"
            -c "source [find interface/kitprog3.cfg]"
            -c "source [find target/psoc6.cfg]"
            )
    add_custom_target(
            openocd-no-rtos
            WORKING_DIRECTORY "${PROJECT_BINARY_DIR}"
            COMMAND ${CYPRESS_OPENOCD_EXECUTABLE}
            ${OPENOCD_PSOC6_COMMON_OPTIONS}
            USES_TERMINAL
    )

    add_custom_target(
            openocd-rtos
            WORKING_DIRECTORY "${PROJECT_BINARY_DIR}"
            COMMAND ${CYPRESS_OPENOCD_EXECUTABLE}
            ${OPENOCD_PSOC6_COMMON_OPTIONS}
            -c "psoc6.cpu.cm4 configure -rtos FreeRTOS"
            USES_TERMINAL
    )

    add_custom_target(
            openocd-flash
            WORKING_DIRECTORY "${PROJECT_BINARY_DIR}"
            COMMAND ${CYPRESS_OPENOCD_EXECUTABLE}
            ${OPENOCD_PSOC6_COMMON_OPTIONS}
            -c "init"
            -c "halt"
            -c "if [catch {program {${EXECUTABLE_NAME}.elf}}] { echo {** Program operation failed **} } else { echo {** Program operation completed successfully **} }"
            -c "reset init"
            -c "shutdown"
            DEPENDS		${EXECUTABLE_NAME}.elf
            USES_TERMINAL
    )

    if(NOT TARGET openocd-erase)
        add_custom_target(
                openocd-erase
                WORKING_DIRECTORY "${PROJECT_BINARY_DIR}"
                COMMAND ${CYPRESS_OPENOCD_EXECUTABLE}
                ${OPENOCD_PSOC6_COMMON_OPTIONS}
                -c "init"
                -c "halt"
                -c "flash erase_sector 0 0 last"
                -c "flash erase_sector 2 0 last" # Erase whole flash before programming. It ensures that Chain of Trust doesn't compute hash from an leftover from other previously loaded images.
                -c "reset init"
                -c "shutdown"
                USES_TERMINAL
        )
    endif()
endif()