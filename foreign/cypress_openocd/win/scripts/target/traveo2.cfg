#
# Configuration script for Cypress Traveo-II B-E family of microcontrollers.
# Traveo-II B-E is a dual-core device with CM0+ and CM4 cores. Both cores share
# the same Flash/RAM/MMIO address space.
#

source [find target/swj-dp.tcl]
adapter_khz 1000

global _CHIPNAME
if { [info exists CHIPNAME] } {
	set _CHIPNAME $CHIPNAME
} else {
	set _CHIPNAME tv2
}

#
# Is CM0 Debugging enabled ?
#
global _ENABLE_CM0
if { [info exists ENABLE_CM0] } {
	set _ENABLE_CM0 $ENABLE_CM0
} else {
	set _ENABLE_CM0 1
}

#
# Is CM4 Debugging enabled ?
#
global _ENABLE_CM4
if { [info exists ENABLE_CM4] } {
	set _ENABLE_CM4 $ENABLE_CM4
} else {
	set _ENABLE_CM4 1
}

if { $_ENABLE_CM0 } {
	set _ACQUIRE_TARGET cm0
} else {
	set _ACQUIRE_TARGET cm4
}

# Enable Test Mode Acquire (works only with KitProg3 / MiniProg4)
#
global _ENABLE_ACQUIRE
global _ENABLE_POWER_SUPPLY
if { [adapter_name] eq "kitprog3" } {
	if { [info exists ENABLE_ACQUIRE] } {
		set _ENABLE_ACQUIRE $ENABLE_ACQUIRE
	} else {
		set _ENABLE_ACQUIRE 1
	}

	if { [info exists ENABLE_POWER_SUPPLY] } {
		set _ENABLE_POWER_SUPPLY $ENABLE_POWER_SUPPLY
	} else {
		set _ENABLE_POWER_SUPPLY 0
	}
} else {
	set _ENABLE_ACQUIRE  0
	set _ENABLE_POWER_SUPPLY 0
	echo "** Test Mode acquire not supported by selected adapter"
}

if { $_ENABLE_ACQUIRE } {
	echo "** Auto-acquire enabled, use \"set ENABLE_ACQUIRE 0\" to disable"
	kitprog3 acquire_config on 3 0 2
}

if { $_ENABLE_POWER_SUPPLY } {
	echo "** Enabling target power ($_ENABLE_POWER_SUPPLY mV) \"set ENABLE_POWER_SUPPLY 0\" to disable"
	kitprog3 power_config on $_ENABLE_POWER_SUPPLY
}

global _WORKAREASIZE_CM0
if { [info exists WORKAREASIZE_CM0] } {
	set _WORKAREASIZE_CM0 $WORKAREASIZE_CM0
} else {
	set _WORKAREASIZE_CM0 0x8000
}

global _WORKAREASIZE_CM4
if { [info exists WORKAREASIZE_CM4] } {
	set _WORKAREASIZE_CM4 $WORKAREASIZE_CM4
} else {
	set _WORKAREASIZE_CM4 0x8000
}

global _WORKAREAADDR_CM0
if { [info exists WORKAREAADDR_CM0] } {
	set _WORKAREAADDR_CM0 $WORKAREAADDR_CM0
} else {
	set _WORKAREAADDR_CM0 0x08000000
}

global _WORKAREAADDR_CM4
if { [info exists WORKAREAADDR_CM4] } {
	set _WORKAREAADDR_CM4 $WORKAREAADDR_CM4
} else {
	set _WORKAREAADDR_CM4 0x08000000
}

global TARGET
set TARGET $_CHIPNAME.cpu
swj_newdap $_CHIPNAME cpu -irlen 4 -ircapture 0x1 -irmask 0xf
dap create $_CHIPNAME.dap -chain-position $_CHIPNAME.cpu

proc init_reset { mode } {
	global RESET_MODE
	set RESET_MODE $mode

	if {[using_jtag]} {
		jtag arp_init-reset
	}
}

# Utility to make 'reset halt' work as reset;halt on a target
# It does not prevent running code after reset
proc psoc6_deassert_post { target } {
	global _ENABLE_ACQUIRE
	global _ACQUIRE_TARGET
	global RESET_MODE

	if { $RESET_MODE ne "run" } {
		if { [string match "*${_ACQUIRE_TARGET}" $target] } {
			if { $_ENABLE_ACQUIRE } {
				catch { acquire psoc6.cpu.${_ACQUIRE_TARGET} }
			}

			catch {
				echo "** SFlash SiliconID:   0x[format %08X [mrw 0x17000000]]"
				echo "** Flash Boot version: 0x[format %08X [mrw 0x17002004]]"
				echo "** Chip Protection: [chip_protection]"
			}
		}

		# Traveo-II cleared AP registers including TAR during reset
		# Force examine to synchronize OpenOCD target status
		$target arp_examine
		$target arp_poll
		$target arp_poll
		set st [$target curstate]

		if { $st eq "reset" } {
			# we assume running state follows
			# if reset accidentally halts, waiting is useless
			catch { $target arp_waitstate running 100 }
			set st [$target curstate]
		}

		if { $st eq "running" } {
			echo "** $target: Ran after reset and before halt..."
			if [string match "*${_ACQUIRE_TARGET}" $target ] {
				if { $_ENABLE_ACQUIRE == 0 } {
					sleep 100
					psoc6 reset_halt
				} else {
					$target arp_halt
				}
			} else {
				$target arp_halt
			}
			$target arp_waitstate halted 100
		}
	}
}

if { $_ENABLE_CM0 } {
	target create ${TARGET}.cm0 cortex_m -dap $_CHIPNAME.dap -ap-num 1 -coreid 0
	${TARGET}.cm0 configure -work-area-phys $_WORKAREAADDR_CM0 -work-area-size $_WORKAREASIZE_CM0 -work-area-backup 0

	flash bank ${_CHIPNAME}_main_cm0		psoc6 0x10000000 0 0 0 ${TARGET}.cm0
	flash bank ${_CHIPNAME}_work_cm0		psoc6 0x14000000 0 0 0 ${TARGET}.cm0
	flash bank ${_CHIPNAME}_super_cm0		psoc6 0x17000000 0 0 0 ${TARGET}.cm0

	${TARGET}.cm0 cortex_m reset_config sysresetreq
	${TARGET}.cm0 configure -event reset-deassert-post "psoc6_deassert_post ${TARGET}.cm0"
	${TARGET}.cm0 configure -event gdb-attach "${TARGET}.cm0 arp_halt; ${TARGET}.cm0 arp_waitstate halted 100"

	add_verify_range ${TARGET}.cm0 0x08000000 0x800000
	add_verify_range ${TARGET}.cm0 0x10000000 0x800000
	add_verify_range ${TARGET}.cm0 0x14000000 0x800000
	add_verify_range ${TARGET}.cm0 0x17000000 0x800000
}

if { $_ENABLE_CM4 } {
	target create ${TARGET}.cm4 cortex_m -dap $_CHIPNAME.dap -ap-num 2 -coreid 1
	${TARGET}.cm4 configure -work-area-phys $_WORKAREAADDR_CM4 -work-area-size $_WORKAREASIZE_CM4 -work-area-backup 0

	if { $_ENABLE_CM0 } {
		flash bank ${_CHIPNAME}_main_cm4		virtual 0x10000000 0 0 0 ${TARGET}.cm4 ${_CHIPNAME}_main_cm0
		flash bank ${_CHIPNAME}_work_cm4		virtual 0x14000000 0 0 0 ${TARGET}.cm4 ${_CHIPNAME}_work_cm0
		flash bank ${_CHIPNAME}_super_cm4		virtual 0x17000000 0 0 0 ${TARGET}.cm4 ${_CHIPNAME}_super_cm0
	} else {
		flash bank ${_CHIPNAME}_main_cm4		psoc6 0x10000000 0 0 0 ${TARGET}.cm4
		flash bank ${_CHIPNAME}_work_cm4		psoc6 0x14000000 0 0 0 ${TARGET}.cm4
		flash bank ${_CHIPNAME}_super_cm4		psoc6 0x17000000 0 0 0 ${TARGET}.cm4
	}

	add_verify_range ${TARGET}.cm4 0x08000000 0x800000
	add_verify_range ${TARGET}.cm4 0x10000000 0x800000
	add_verify_range ${TARGET}.cm4 0x14000000 0x800000
	add_verify_range ${TARGET}.cm4 0x17000000 0x800000

	${TARGET}.cm4 configure -event reset-deassert-post "psoc6_deassert_post ${TARGET}.cm4"
	${TARGET}.cm4 configure -event gdb-attach "${TARGET}.cm4 arp_halt;${TARGET}.cm4 arp_waitstate halted 100"

	if { $_ENABLE_CM0 } {
		# Use soft reset on dual-core devices
		${TARGET}.cm4 cortex_m reset_config vectreset
		targets ${TARGET}.cm0
	} else {
		# Use harder reset on single-core devices
		${TARGET}.cm4 cortex_m reset_config sysresetreq
	}
}

if {[using_jtag]} {
	if { [info exists TRAVEOII_PSVP] } {
		swj_newdap $_CHIPNAME bs -irlen 18 -expected-id 0x00000000
	} else {
		swj_newdap $_CHIPNAME bs -irlen 4 -expected-id 0x1e3000d3
	}
	adapter_nsrst_delay 100
}

proc mrw { ADDR } {
	set foo(0) 0
	if ![ catch { mem2array foo 32 $ADDR 1  } msg ] {
		return $foo(0)
	} else {
		error $msg
	}
}

proc chip_protection {} {
	set protection [ expr [mrw 0x402020C4]]
	set ret "X"

	switch $protection {
		1 { set ret "VIRGIN" }
		2 { set ret "NORMAL" }
		3 { set ret "SECURE" }
		4 { set ret "DEAD" }
		default { set ret "UNKNOWN" }
	}

	return $ret
}

proc acquire { target } {
	global _ENABLE_ACQUIRE
	if { $_ENABLE_ACQUIRE == 0 } {
		echo "----------------------------------------------------------------"
		echo "Test Mode acquire disabled. Use 'set ENABLE_ACQUIRE 1' to enable"
		echo "----------------------------------------------------------------"
		error
	}

	# acquire will leave CPU in running state
	# openocd does not expect this
	kitprog3 acquire_psoc

	# we need to re-examine and halt target manually
	${target} arp_examine
	${target} arp_poll
	${target} arp_poll

	# Ensure target has stopped on WFI instruction
	set loops 200
	while { $loops } {
		set sleeping [ expr [mrw 0xE000EDF0] & 0x00040000 ]
		if { $sleeping } break
		set loops [ expr $loops - 1 ]
		sleep 10
	}

	if { $sleeping } {
		${target} arp_halt
		${target} arp_waitstate halted 100
		echo "** Device acquired successfully"
		return
	}

	echo "--------------------------------------------"
	echo "Failed to acquire Traveo-II device in Test Mode"
	echo "--------------------------------------------"
	error
}

add_usage_text acquire "target (e.g. psoc6.cpu.cm0)"
add_help_text acquire "Acquires Traveo-II device in Test Mode"

proc detect_smif {{sflash_base 0x17000000}} {
  global _CHIPNAME
  global TARGET
  set cfg_ptr  [mrw [mrw [ expr $sflash_base + 62 * 512 + 0x0C ]]]
  if { $cfg_ptr == 0 || $cfg_ptr == 0xFFFFFFFF  || $cfg_ptr < 0x10000000 || $cfg_ptr > 0x10200000 } {
	echo "** SMIF configuration structure not found or invalid"
	return
  }
  set chip_num [mrw $cfg_ptr]
  set chip_cfg_arry_p [mrw [expr $cfg_ptr + 4]]

  echo ""
  for {set i 0} {$i < $chip_num} {incr i} {
	set chip_cfg  [mrw [expr $chip_cfg_arry_p + 4 * $i]]
	set region_base [mrw [expr $chip_cfg + 12]]
	set region_size [mrw [expr $chip_cfg + 16]]
	set phys_cfg    [mrw [expr $chip_cfg + 24]]
	set erase_size  [mrw [expr $phys_cfg + 24]]
	set prgm_size   [mrw [expr $phys_cfg + 36]]

	echo "### SMIF region #${i} - Erase Size: 0x[format %X $erase_size], Program Size: 0x[format %X $prgm_size]"
	echo "flash bank \$\{_CHIPNAME\}_smif${i}_cm0 psoc6 0x[format %08X $region_base] 0x[format %08X $region_size] 4 4 \$\{TARGET\}.cm0 cmsis_algorithm flm/cypress/psoc6/CY8C6xxx_SMIF.elf 0x1000\n"
  }
}

add_usage_text detect_smif "sflash_base (optional, 0x17000000 by default)"
add_help_text detect_smif "Detects SMIF regions and displays flash bank configuration"

proc erase_all {} {
	lset banks [flash list]

	for {set i [expr [llength $banks] - 1]} { $i >= 0 } { set i [expr $i - 1]} {
		set bank [lindex $banks $i]
		if { $bank(name) != "virtual" } {
			flash erase_sector $i 0 last
		}
	}
}

add_help_text erase_all "Erases all non-virtual flash banks (in reverse order, for SMIF compatibility)"
