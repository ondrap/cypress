source [find target/swj-dp.tcl]
adapter_khz 1000

global _CHIPNAME
if { [info exists CHIPNAME] } {
	set _CHIPNAME $CHIPNAME
} else {
	set _CHIPNAME tv28m
}

global TARGET
set TARGET $_CHIPNAME.cpu
swj_newdap $_CHIPNAME cpu -irlen 4 -ircapture 0x1 -irmask 0xf
dap create $_CHIPNAME.dap -chain-position $_CHIPNAME.cpu

$_CHIPNAME.dap apsel 2
$_CHIPNAME.dap apcsw [expr (1 << 24) | (1 << 25) | (1 << 29) | (1 << 31)]
$_CHIPNAME.dap apsel 3
$_CHIPNAME.dap apcsw [expr (1 << 24) | (1 << 25) | (1 << 29) | (1 << 31)]

proc init_reset { mode } {
	global RESET_MODE
	set RESET_MODE $mode

	if {[using_jtag]} {
		jtag arp_init-reset
	}
}

proc mrw { ADDR } {
	set foo(0) 0
	if ![ catch { mem2array foo 32 $ADDR 1  } msg ] {
		return $foo(0)
	} else {
		error $msg
	}
}

proc chip_protection {} {
	set protection [ expr [mrw 0x402020C4]]
	set ret "X"

	switch $protection {
		1 { set ret "VIRGIN" }
		2 { set ret "NORMAL" }
		3 { set ret "SECURE" }
		4 { set ret "DEAD" }
		default { set ret "UNKNOWN" }
	}

	return $ret
}

# Utility to make 'reset halt' work as reset;halt on a target
# It does not prevent running code after reset
proc reset_deassert_post { target } {
	global RESET_MODE
	$target arp_examine
	$target arp_poll
	$target arp_poll

	if { $RESET_MODE ne "run" } {
		if { [string match "*cm0" $target] } {
			catch {
				echo "** Device SiliconID:   0x[format %08X [mrw 0x17000000]]"
				echo "** Flash Boot version: 0x[format %08X [mrw 0x17002004]]"
				echo "** Chip Protection: [chip_protection]"
			}
		}

		set st [$target curstate]
		if { $st eq "reset" } {
			# we assume running state follows
			# if reset accidentally halts, waiting is useless
			catch { $target arp_waitstate running 100 }
			set st [$target curstate]
		}

		if { $st eq "running" } {
			echo "$target: Ran after reset and before halt..."
			if [string match "*cm0" $target ] {
				sleep 100
				psoc6 reset_halt
			} else {
				$target arp_halt
			}

			$target arp_waitstate halted 100
		}
	}
}

proc enable_cm7x {} {
	mww 0x40261244 0x80000000
	mww 0x40261248 0x80000000
	mww 0x4020040C 15
	mww 0x4020000C 15
	mww 0x40201200 0x05FA0001
	mww 0x40201200 0x05FA0003
	mww 0x40201210 0x05FA0001
	mww 0x40201210 0x05FA0003
}

target create ${TARGET}.cm0  cortex_m -dap $_CHIPNAME.dap -ap-num 1 -coreid 0
${TARGET}.cm0 configure -work-area-phys 0x28000000 -work-area-size 0x4000 -work-area-backup 0
${TARGET}.cm0 cortex_m reset_config sysresetreq
${TARGET}.cm0 configure -event examine-end { enable_cm7x }
${TARGET}.cm0 configure -event reset-deassert-post "enable_cm7x; catch {reset_deassert_post ${TARGET}.cm0}"

target create ${TARGET}.cm70 cortex_m -dap $_CHIPNAME.dap -ap-num 2 -coreid 1
${TARGET}.cm70 cortex_m reset_config vectreset
${TARGET}.cm70 configure -event reset-deassert-post "catch {reset_deassert_post ${TARGET}.cm70}"

target create ${TARGET}.cm71 cortex_m -dap $_CHIPNAME.dap -ap-num 3 -coreid 2
${TARGET}.cm71 cortex_m reset_config vectreset
${TARGET}.cm71 configure -event reset-deassert-post "catch {reset_deassert_post ${TARGET}.cm71}"

flash bank ${_CHIPNAME}_main0_cm0		psoc6 0x10000000 0x100000 0 0 ${TARGET}.cm0
flash bank ${_CHIPNAME}_main1_cm0		psoc6 0x10400000 0x100000 0 0 ${TARGET}.cm0
flash bank ${_CHIPNAME}_work_cm0		psoc6 0x14000000 0 0 0 ${TARGET}.cm0
flash bank ${_CHIPNAME}_super_cm0		psoc6 0x17000000 0 0 0 ${TARGET}.cm0

targets ${TARGET}.cm0
adapter_nsrst_delay 100
