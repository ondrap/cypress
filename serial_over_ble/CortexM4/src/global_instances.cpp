//
// Created on 6/7/2019.
//

#include "shared/drivers/uart.hpp"

namespace shared{
namespace drivers{

extern class UART uart;

static configuration::UART const uartConfiguration = {
  .scb = SCB5,
  .tx = {
    .port = GPIO_PRT5,
    .pin  = P5_1_NUM,
    .hsiom = P5_1_SCB5_UART_TX,
  },
  .rx = {
    .port = GPIO_PRT5,
    .pin  = P5_0_NUM,
    .hsiom = P5_0_SCB5_UART_RX,
  },
  .clock = {
    .destination = PCLK_SCB5_CLOCK,
    .divider = {
      .type = CY_SYSCLK_DIV_16_BIT,
      .number = 0,
      .division = 54,
    },
  },
  .interrupt = {
    .source = scb_5_interrupt_IRQn,
    .priority = 7,
  },
  .uartIsr = UART::C_CALLBACKS<uart>::UARTIsr,
};

UART uart(uartConfiguration);

}}

void InitSystem(){
  shared::drivers::uart.Init();
}


