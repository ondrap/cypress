#include <stdio.h>
#include "cy_syslib.h"
#include "cy_gpio.h"
#include "serial_over_ble/CortexM4/src/global_instances.hpp"

int main(void)
{
  __enable_irq(); /* Enable global interrupts. */

  setvbuf(stdin, NULL, _IONBF, 0);

  InitSystem();

  for(;;){
    printf("Ahoj\r\n");
    Cy_SysLib_Delay(500);
  }
}

/* [] END OF FILE */
