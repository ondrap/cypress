//
// Created on 6/5/2019.
//

#include "cy_systick.h"
#include "cy_syspm.h"
#include "cy_gpio.h"
#include "cy_sysclk.h"

extern "C" void Cy_SystemInit()
{
  /* Set worst case memory wait states (150 MHz), ClockInit() will update */
  Cy_SysLib_SetWaitStates(false, 150);

  if(0u == Cy_SysLib_GetResetReason()) /* POR, XRES, or BOD */
  {
    Cy_SysLib_ResetBackupDomain();
  }

  /* Power Mode */
  Cy_SysPm_LdoSetVoltage(CY_SYSPM_LDO_VOLTAGE_1_1V);

  /* PMIC Control */
  Cy_SysPm_UnlockPmic();
  Cy_SysPm_DisablePmicOutput();

  //Initialize clock subsystem
  uint32_t status;

  Cy_SysClk_ClkLfSetSource(CY_SYSCLK_CLKLF_IN_WCO);

  /* Configure CPU clock dividers */
  Cy_SysClk_ClkFastSetDivider(0u);
  Cy_SysClk_ClkPeriSetDivider(1u);
  Cy_SysClk_ClkSlowSetDivider(0u);

  /* Configure LF & HF clocks */
  // Fast and peripheral clocks
  Cy_SysClk_ClkHfSetSource(0u, CY_SYSCLK_CLKHF_IN_CLKPATH1);
  Cy_SysClk_ClkHfSetDivider(0u, CY_SYSCLK_CLKHF_NO_DIVIDE);
  Cy_SysClk_ClkHfEnable(0u);

  /* Configure Path Clocks */
  Cy_SysClk_ClkPathSetSource(0, CY_SYSCLK_CLKPATH_IN_IMO);
  Cy_SysClk_ClkPathSetSource(1, CY_SYSCLK_CLKPATH_IN_IMO);
  Cy_SysClk_ClkPathSetSource(2, CY_SYSCLK_CLKPATH_IN_IMO);
  Cy_SysClk_ClkPathSetSource(3, CY_SYSCLK_CLKPATH_IN_IMO);
  Cy_SysClk_ClkPathSetSource(4, CY_SYSCLK_CLKPATH_IN_IMO);

  {
    // PLL frequency formula:
    // Freq = ((PathMux1 * feedback) / reference) / out
    // 150MHz = ((8 * 75) / 2) / 2
    const cy_stc_pll_manual_config_t pllConfig =
      {
        .feedbackDiv  = 75u,
        .referenceDiv = 2u,
        .outputDiv    = 2u,
        .lfMode       = false,
        .outputMode   = CY_SYSCLK_FLLPLL_OUTPUT_AUTO
      };
    status = Cy_SysClk_PllManualConfigure(1u, &pllConfig);
    if (CY_SYSCLK_SUCCESS != status)
    {
      while(true);
    }
  }
  status = Cy_SysClk_PllEnable(1u, 10000u);
  if (CY_SYSCLK_SUCCESS != status)
  {
    while(true);
  }

  /* Configure miscellaneous clocks */
  Cy_SysClk_ClkTimerSetSource(CY_SYSCLK_CLKTIMER_IN_IMO);
  Cy_SysClk_ClkTimerSetDivider(0);
  Cy_SysClk_ClkTimerEnable();
  Cy_SysClk_ClkPumpSetSource(CY_SYSCLK_PUMP_IN_CLKPATH0);
  Cy_SysClk_ClkPumpSetDivider(CY_SYSCLK_PUMP_DIV_4);
  Cy_SysClk_ClkPumpEnable();
  Cy_SysClk_ClkBakSetSource(CY_SYSCLK_BAK_IN_WCO);
  Cy_SysTick_SetClockSource(CY_SYSTICK_CLOCK_SOURCE_CLK_LF);

  /* Disable clocks started by default */
  Cy_SysClk_IloDisable();

  Cy_SysLib_SetWaitStates(false, 150);
}