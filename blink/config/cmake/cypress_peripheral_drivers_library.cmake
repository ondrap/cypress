if(NOT DEFINED CYPRESS_PERIPHERAL_DRIVERS_LIBRARY)
    message(SEND_ERROR "CYPRESS_PERIPHERAL_DRIVERS_LIBRARY isn't set.")
endif()

set ( CYPRESS_PERIPHERAL_DRIVERS_LIBRARY_INCLUDE_DIRS
    ${CYPRESS_PERIPHERAL_DRIVERS_LIBRARY}/cmsis/include
    ${CYPRESS_PERIPHERAL_DRIVERS_LIBRARY}/drivers/include
    ${CYPRESS_PERIPHERAL_DRIVERS_LIBRARY}/devices/include
    ${CYPRESS_PERIPHERAL_DRIVERS_LIBRARY}/devices/templates/COMPONENT_MTB
)

set (CYPRESS_PERIPHERAL_DRIVERS_LIBRARY_SRCS
    ${CYPRESS_PERIPHERAL_DRIVERS_LIBRARY}/drivers/source/cy_syslib.c
    ${CYPRESS_PERIPHERAL_DRIVERS_LIBRARY}/drivers/source/cy_gpio.c
    ${CYPRESS_PERIPHERAL_DRIVERS_LIBRARY}/drivers/source/cy_sysclk.c
    ${CYPRESS_PERIPHERAL_DRIVERS_LIBRARY}/drivers/source/cy_device.c
    ${CYPRESS_PERIPHERAL_DRIVERS_LIBRARY}/drivers/source/cy_sysint.c
    ${CYPRESS_PERIPHERAL_DRIVERS_LIBRARY}/drivers/source/cy_wdt.c
    ${CYPRESS_PERIPHERAL_DRIVERS_LIBRARY}/drivers/source/cy_flash.c
    ${CYPRESS_PERIPHERAL_DRIVERS_LIBRARY}/drivers/source/cy_ipc_pipe.c
    ${CYPRESS_PERIPHERAL_DRIVERS_LIBRARY}/drivers/source/cy_ipc_sema.c
    ${CYPRESS_PERIPHERAL_DRIVERS_LIBRARY}/drivers/source/cy_ipc_drv.c
    ${CYPRESS_PERIPHERAL_DRIVERS_LIBRARY}/drivers/source/TOOLCHAIN_GCC_ARM/cy_syslib_gcc.S
)
