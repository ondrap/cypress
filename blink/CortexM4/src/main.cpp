#include "cy_syslib.h"
#include "cy_gpio.h"

int main(void)
{
  __enable_irq(); /* Enable global interrupts. */
  Cy_GPIO_Pin_FastInit(P7_1_PORT, P7_1_NUM, CY_GPIO_DM_STRONG_IN_OFF, 1, P7_1_GPIO);

  for(;;){
    Cy_GPIO_Inv(P7_1_PORT, P7_1_NUM);
    Cy_SysLib_Delay(500);
  }
}

/* [] END OF FILE */
