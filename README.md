# Cypress PSoC63 C++ example projects
Goal of this repository is to provide simple examples how to develop C++ code 
for a Cypress PSoC63 line on different platforms. 
Multiplatform development is achieved with the [CMake](ttps://cmake.org/) and [ARM toolchain](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm).

## Tested HW
 - [PSoC 6 BLE kit](https://www.cypress.com/documentation/development-kitsboards/psoc-6-ble-prototyping-kit-cy8cproto-063-ble)
    - board programer has to be updated on the kitprog3
## How to setup Windows
 - Install following dependecies:
    - arm none eabi toolchain windows build
    - [MSYS2](https://www.msys2.org/)
      - CMake
      - Make     
 - Clion or other IDE
 - Configure Clion to work with the [MinGW](https://www.jetbrains.com/help/clion/quick-tutorial-on-configuring-clion-on-windows.html)
 - Open example project in a IDE
### How to setup Clion to debug an example
 - Open *Edit configuration*
 - Add GDB remote debug
    - set GDB path to the arm-none-eabi-gdb.exe path
    - set 'target-remote' args 
        - for CortexM0+ *:3333*
        - for CortexM4 *:3334*
    - set symbol file to the project.elf path

### Usable project targets
| Name | Description |
| ---- | ----------- |       
| blink/ble etc | Builds all files for both cores and merge them in single elf file |
| openocd | Attach openocd to a connected board (First step before debugging)|
| openocd-flash | Flash elf file in a connected board |
| gdb-CortexM0+ | Debug CortexM0+ core (prerequisity is connected openocd) |
| gdb-CortexM4 | Debug CortexM4 core (prerequisity is connected openocd) |
     